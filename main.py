import os
import time

# Change the desktop background
def change_background(current_time_of_day, image_name):
    current_dir = os.path.dirname(os.path.realpath(__file__))
    image_path = "file://" + current_dir + "/images/" + current_time_of_day + "/" + image_name

    os_name = os.uname().sysname
    if(os_name == "Linux"):
        os.system("/usr/bin/gsettings set org.gnome.desktop.background picture-uri '" + image_path + "'")

# Returns the time of as a string ("morning", "day", etc)
def get_time_of_day():
    current_hour = time.localtime().tm_hour
    
    if(current_hour >= 5 and current_hour <= 11):
        return "morning"
    elif(current_hour >= 12 and current_hour <= 17):
        return "day"
    elif(current_hour >= 18 and current_hour <= 20):
        return "afternoon"
    elif(current_hour >= 21 or current_hour <= 4):
        return "night"

# Directory of this file
current_dir = os.path.dirname(os.path.realpath(__file__))
# Main loop
while(True):
    # Create an array of images for the current time of day
    file_names = []
    current_time_of_day = get_time_of_day()
    for (path, dir_name, file_name) in os.walk(current_dir + "/images/" + current_time_of_day):
        file_names = file_name

    # Change the background for the current time of day every 30 seconds
    for file_name in file_names:
        change_background(current_time_of_day, file_name)
        time.sleep(30)
        continue